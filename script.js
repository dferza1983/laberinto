var listInfo = [
	{
		'id': 1,
		'nombre': 'Yamaha',
		'llantas': [
			{
				'id': 1,
				'tamano': 'Grande'
			},
			{
				'id': 2,
				'tamano': 'Mediano'
			},
			{
				'id': 3,
				'tamano': 'Pequeño'
			}
		]
	},
	{
		'id': 2,
		'nombre': 'Zusuki',
		'llantas': [
			{
				'id': 1,
				'tamano': 'Grande'
			},
			{
				'id': 2,
				'tamano': 'Mediano'
			},
			{
				'id': 3,
				'tamano': 'Pequeño'
			}
		]
	}
]


var matrizN = [
	[0, 0, 0, 1, 1],
	[0, 0, 1, 1, 1],
	[0, 0, 1, 0, 1],
	[0, 0, 1, 0, 1],
	[0, 0, 3, 0, 0]
];

var isCreating = false;
var isEditing = false;

$(document).ready(function () {

	$('#btnNuevo').click(function (e) {
		if (!isCreating) {
			$('#btnNuevo').html('Ocultar');
			isCreating = true;
			showCru();
		} else {
			$('#btnNuevo').html('Nuevo');
			isCreating = false;
			hideCru();
		}
	});
	$('#btnAgregar').click(function (e) {
		create();
	});
	$('#btnEditar').click(function (e) {
		edit();
	});
	buildTable();

	matrizN.forEach(
		(row) => {
			row.forEach(
				(col) => {
					console.log(col)
				}
			)
		}
	)

	for (var i = 0; i < matrizN.length; i++) {
		for (var j = 0; j < matrizN[i].length; j++) {
			console.log("for N" + matrizN[i][j]);
		}
	}

	solve();

});

function buildTable() {
	//$('#tableInfo').html(msg);
	var table = '';
	table += '<tr><th>Id</th><th>Nombre</th><th>Número de llantas</th><th>Opciones</th></tr>'

	listInfo.forEach(
		(listItem) => {
			table += '<tr>' + '<td>' + listItem.id + '</td>' + '<td>'
				+ listItem.nombre + '</td>' + '<td>' + listItem.llantas.length + '</td>'
				+ '<td><button class="button button2" onclick="viewItem(' + listItem.id + ')">Ver</button><button class="button button2" onclick="deleteItem(' + listItem.id + ')">Eliminar</button></td>' + '</tr>'
		}
	)

	$('#tableInfo').html(table);

}

function edit() {
	var existingId = $('#carId').val();
	var newName = $('#carName').val();
	var newNumberLl = $('#carNumberLL').val();

	var llTo = [];

	for (var i = 0; i < Number(newNumberLl); i++) {
		llTo.push(
			{
				'id': i,
				'tamano': 'Grande'
			}
		)
	}

	itemToEdit = listInfo.find(
		(itemTo) => {
			return itemTo.id === Number(existingId)
		}
	)

	listInfo[listInfo.indexOf(itemToEdit)] = {
		'id': Number(existingId),
		'nombre': newName,
		'llantas': llTo
	}

	buildTable();

}

function create() {
	var newName = $('#carName').val();
	var newNumberLl = $('#carNumberLL').val();

	var llTo = [];

	for (var i = 0; i < newNumberLl; i++) {
		llTo.push(
			{
				'id': i,
				'tamano': 'Grande'
			}
		)
	}

	listInfo.push(
		{
			'id': listInfo.length + 1,
			'nombre': newName,
			'llantas': llTo
		}
	)

	buildTable();
	showCru();
}

function deleteItem(item) {
	itemToDelete = listInfo.find(
		(itemTo) => {
			return itemTo.id === item
		}
	)

	listInfo.splice(listInfo.indexOf(itemToDelete), 1);

	buildTable();
}

function viewItem(item) {
	itemToView = listInfo.find(
		(itemTo) => {
			return itemTo.id === item
		}
	)

	$('#cruMain').css('display', 'block');
	$('#btnEditar').css('display', 'block');
	$('#carId').css('display', 'block');
	$('#btnAgregar').css('display', 'none');

	$('#carId').val(itemToView.id);
	$('#carName').val(itemToView.nombre);
	$('#carNumberLL').val(itemToView.llantas.length);
}

function showCru() {
	$('#cruMain').css('display', 'block');
	$('#carId').css('display', 'none');
	$('#btnEditar').css('display', 'none');
	$('#btnAgregar').css('display', 'block');
	$('#carId').val('');
	$('#carName').val('');
	$('#carNumberLL').val('');
}

function hideCru() {
	$('#cruMain').css('display', 'none');
	$('#btnEditar').css('display', 'none');
}

function solve() {

	debugger;

	console.log(new Date().getMilliseconds());

	var actuallyDirection = 'b'; //l: left, r:right, t:top, b:bottom 
	var isSolved = false;
	var actuallyPosition = [0, 4];	
	var path = [];

	while(!isSolved){
		path.push(actuallyPosition[0] + ',' + actuallyPosition[1]);
		if(matrizN[actuallyPosition[0]][actuallyPosition[1]] === 3){
			isSolved = true;
			console.log(path);
			console.log(new Date().getMilliseconds());
		} else {
			switch(actuallyDirection){
				case 'l': {
					if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
						actuallyDirection = 't';
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
					} else if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
						actuallyDirection = 'b';
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
						actuallyDirection = 'r';
					}
					break;
				}
				case 'r': {
					if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
						actuallyDirection = 'b';
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
					} else if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
						actuallyDirection = 't';
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
						actuallyDirection = 'l';
					}
					break;
				}
				case 't': {
					if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
						actuallyDirection = 'r';
					} else if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
						actuallyDirection = 'l';
					} else if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
						actuallyDirection = 'b';
					}
					break;
				}
				case 'b': {
					if(matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] + 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] + 1];
						actuallyDirection = 'l';
					} else if(matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] + 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] + 1, actuallyPosition[1]];
					} else if(matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 1 || matrizN[actuallyPosition[0]][actuallyPosition[1] - 1] === 3){
						actuallyPosition = [actuallyPosition[0], actuallyPosition[1] - 1];
						actuallyDirection = 'r';
					} else if(matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 1 || matrizN[actuallyPosition[0] - 1][actuallyPosition[1]] === 3){
						actuallyPosition = [actuallyPosition[0] - 1, actuallyPosition[1]];
						actuallyDirection = 't';
					}
					break;
				}
			}
		}
	}

}
